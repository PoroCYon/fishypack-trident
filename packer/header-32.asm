
;%define USE_GZIP

jumptar2_addr equ 0x51

%define STDIN_FILENO	0
%define STDOUT_FILENO	1
%define STDERR_FILENO	2

%define SYS_memfd_create	356
%define SYS_fork			2
%define SYS_waitid			284
%define SYS_execve			11
%define SYS_open			5
%define SYS_lseek			19
%define SYS_dup2			63

bits 32

	org 0x00200000

ehdr:
	db 0x7f,"ELF"	; e_ident
	; jg short jumptar
	; dec esp
	; inc esi

	; machine numbers, padding
_start.0:
	mov ax, SYS_memfd_create
	mov ebx, esp
	int 0x80

	mov al, SYS_fork
	pop ecx

			  ; e_type
	jmp short ehdr.e_machine.mid
	db	0
	;dw	2			;!e_type
	db	3
ehdr.e_machine.mid:
	add dl, dh ; 0x00 0xf2 ; benign garbage instruction
	;dw	3			;!e_machine
	int 0x80
	;dd	0			; e_version
	db 0xEB ; jmp short jumptar2
	dd	_start	;!e_entry
	dd	phdr - ehdr	;!e_phoff

phdr:
	dd	1			; e_shoff	;!p_type
	dd	0			; e_flags	;!p_offset
	dd	ehdr		; e_ehsize	;!p_vaddr
					;!e_phentsize
	dw	1			;!e_phnum	; p_paddr
	;; TODO: put code here ; except that haxoring the filesize doesn't quite work
	;; => TODO: try with phdr more at the end of the ehdr
	dw	0			; e_shentsize
	dd	filesize	; e_shnum	;~p_filesz	; can be larger than needed, but must be at least filesize
					; e_shstrndx
ehdr.end:
	dd	filesize				;~p_memsz	; ^

_start:
	jmp short _start.0

_child:
	;~p_flags
	mov ebx, __self
		        ; p_align
	;dd	5						;~p_flags	; MUST have AT LEAST 4 or 1 set
	mov edi, ebx ; back up for (much) later
	;dd	0						; p_align
	dec ecx ; 0 == O_RDONLY
	mov al, SYS_open
	int 0x80

	;fd1
	push eax

	; seek
	mov al, SYS_lseek
	pop ebx
	push ebx
	mov cl, filesize
	int 0x80

	jmp short _child.2

;	times (0x47-($-ehdr)) db 0
;jumptar:
	;int3

	times (jumptar2_addr-($-ehdr)) db 0
jumptar2:
	test eax, eax
	jz short _child

_parent:
	xor ebx, ebx
	mov ax, SYS_waitid

	mov si, 4
	int 0x80

	; gets pointer to __memfd from stack
	mov ebx, __memfd
	; get environ pointer from stack into rdx
	; assume argc == 1
	mov dl, 16+8
	add edx, esp

	;mov eax, SYS_open
	;; mov ebx, ebx
	;mov ecx, 4
	;int 0x80

	;mov ebx, eax
	;mov eax, 3;SYS_read
	;mov ecx, esp
	;mov edx, filesize
	;int 0x80

	;mov edi, ebx
	;mov ecx, esp
	;mov eax, 4;SYS_write
	;mov ebx, STDERR_FILENO
	;;mov edx, filesize
	;int 0x80

	;mov ebx, edi
	push SYS_execve
	pop eax
	mov ecx, esp ;use our args as args
	int 0x80

_child.2:

	;dup2 demo->stdout
	dec ebx

	mov al, SYS_dup2
	mov cl, STDOUT_FILENO
	int 0x80

	;dup2 self->stdin
	mov al, SYS_dup2
	pop ebx
	dec ecx ; 1 (STDOUT_FILENO) minus 1 equals zero! (STDIN_FILENO)
	int 0x80

	;execve
	;push __zip ; one byte shorter
	mov al, SYS_execve
%ifdef USE_GZIP
	add edi, __zip-__self
	mov ebx, edi
%else
	push ecx
	add edi, __zip-__self
	push edi
	;push __zip ; one byte shorter
	pop ebx
	push ebx
%endif
	mov ecx, esp
	; xor rdx, rdx ;empty environ
	int 0x80

__self:
	db '/proc/self/exe',0
__memfd:
	db '/dev/','fd/3',0
__zip:
%ifdef USE_GZIP
	db '/bin/zcat',0
%else
	db '/usr/bin/xzcat',0
%endif

END:

filesize equ END - ehdr

;%if jumptar-ehdr != 0x47
;%error "Bad jumptar!"
;%endif
%if ehdr.e_machine.mid-ehdr != 0x13
%error "Bad .e_machine.mid"
%endif
%if jumptar2-ehdr != jumptar2_addr
%error "Bad jumptar2"
%endif

payload:
;	incbin "payload.bin"

